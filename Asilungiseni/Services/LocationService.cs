﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Gms.Location;
using Android.Locations;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Asilungiseni.Models;

namespace Asilungiseni.Services
{
    public class LocationService
    {

        public LocationService()
        {            
        }

        public async Task<Coordinates> GetCurrentLocation(Context context)
        {
            try
            {
                FusedLocationProviderClient locationProviderClient = LocationServices.GetFusedLocationProviderClient(context);
               Location location = await locationProviderClient.GetLastLocationAsync();

                // Location location =  Task.Run(async () => { return await locationProviderClient.GetLastLocationAsync(); }).Result;

                if (location != null)
                {
                    return new Coordinates { Latitude = location.Latitude, Longitude = location.Longitude };
                }
            }
            catch (Exception e)
            {
                var error = e.Message;
            }

            return null;
        }

    }
}