﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Xml;
using Asilungiseni.Models;
using System.Web;

using RestSharp;

namespace Asilungiseni.Services
{
    public class UserService
    {
       HttpWebRequest request;

       public UserService()
       {
       }
        
       public async Task<List<string>> GetUsers()
       {
            try
            {
                string url = "http://10.69.153.250/Asilungise/api/IncidentType/IncidentType";
                request = (HttpWebRequest)WebRequest.Create(new Uri(url));
                request.ContentType = "application/xml";
                request.Method = "GET";

                using (WebResponse response = await request.GetResponseAsync())
                {
                    using (Stream stream = response.GetResponseStream())
                    {
                        using (StreamReader sr = new StreamReader(stream))
                        {
                            string strContent = sr.ReadToEnd();
                            StringWriter sw = new StringWriter();
                            HttpUtility.HtmlDecode(strContent, sw);
                            XmlDocument doc = new XmlDocument();
                            doc.LoadXml(sw.ToString());
                            XmlNodeList list = doc.GetElementsByTagName("IncidentTypeModel");
                            List<string> incidenttypes = new List<string>();

                            for (int i = 0; i < list.Count; i++)
                            {
                                for (int j = 0; j < list[i].ChildNodes.Count; j++)
                                {
                                    if (list[i].ChildNodes[j].Name == "Name")
                                    {
                                        incidenttypes.Add(list[i].ChildNodes[j].InnerText);
                                    }                                   
                                }
                            }

                           // response.Wait();

                            return incidenttypes;
                        }                       
                    }

                  
                }
            }


            catch (Exception e)
            {
                var m = e.Message;
            }     


            return null;         
        }

        public async Task<IRestResponse> Login(UserModel user)
        {
            var TaskCompletionSource = new TaskCompletionSource<IRestResponse>();

            try
            {
               //var TaskCompletionSource = new TaskCompletionSource<IRestResponse>();
                var client = new RestClient("http://10.69.153.250/Asilungise/");
                var request = new RestRequest("api/account/login", Method.POST);

                request.AddObject(new UserModel { Email = user.Email, Password = user.Password });

                client.ExecuteAsync(request, (IRestResponse response, RestRequestAsyncHandle handler) =>
                {                  
                    TaskCompletionSource.SetResult(response);
                });

            }

            catch (Exception ex)
            {
                var error = ex.Message;
            }

            return await TaskCompletionSource.Task;
            // return isSuccess;
        }

        public async Task<IRestResponse> RegisterUser(UserModel user)
        {
            try
            {
                var TaskCompletionSource = new TaskCompletionSource<IRestResponse>();
                var client = new RestClient("http://10.12.242.181/Asilungise/"); 
                var request = new RestRequest("api/account/registeruser", Method.POST);
          
                request.AddObject(user);

                //var response = await client.ExecuteTaskAsync(request);

                client.ExecuteAsync(request, (IRestResponse response, RestRequestAsyncHandle handler) => {

                    if (response.ResponseStatus == ResponseStatus.Error)
                    {
                        TaskCompletionSource.SetException(response.ErrorException);
                    }

                    else
                    {
                        TaskCompletionSource.SetResult(response);
                    }
                });

                return await TaskCompletionSource.Task;
            }

            catch (Exception ex)
            {
                var error = ex.Message;
                return null;
            }
        }
    }
}