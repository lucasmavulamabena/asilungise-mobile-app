﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Xml;
using Asilungiseni.Models;
using System.Web;

using RestSharp;
using System.Net.Http;
using System.Text;

namespace Asilungiseni.Services
{
    public class IncidentService
    {
       HttpWebRequest request;

       public IncidentService()
       {
       }
        
       public async Task<List<string>> GetIncidentTypes()
       {
            try
            {
                string url = "http://10.113.11.10/Asilungise/api/IncidentType/IncidentType";
               // string url = "http://localhost/Asilungise/api/IncidentType/IncidentType";
                request = (HttpWebRequest)WebRequest.Create(new Uri(url));
                request.ContentType = "application/xml";
                request.Method = "GET";

                using (WebResponse response = await request.GetResponseAsync())
                {
                    using (Stream stream = response.GetResponseStream())
                    {
                        using (StreamReader sr = new StreamReader(stream))
                        {
                            string strContent = sr.ReadToEnd();
                            StringWriter sw = new StringWriter();
                            HttpUtility.HtmlDecode(strContent, sw);
                            XmlDocument doc = new XmlDocument();
                            doc.LoadXml(sw.ToString());
                            XmlNodeList list = doc.GetElementsByTagName("IncidentTypeModel");
                            List<string> incidenttypes = new List<string>();

                            for (int i = 0; i < list.Count; i++)
                            {
                                for (int j = 0; j < list[i].ChildNodes.Count; j++)
                                {
                                    if (list[i].ChildNodes[j].Name == "Name")
                                    {
                                        incidenttypes.Add(list[i].ChildNodes[j].InnerText);
                                    }                                   
                                }
                            }

                            // response.Wait();
                            return incidenttypes;
                        }                       
                    }                  
                }
            }


            catch (Exception e)
            {
                var m = e.Message;
            }     


            return null;         
        }

        public async Task<IRestResponse> Report(ReportIncidentModel incidentmodel, /*System.IO.Stream file*/ byte[] file)
        {
            try
            {
                var TaskCompletionSource = new TaskCompletionSource<IRestResponse>();
                var client = new RestClient("http://localhost/Asilungise/");           

                var request = new RestRequest("api/Incident/ReportIncident", Method.POST);

                if (file != null)
                {
                    request.AddFileBytes("Incident Image", file, "incidentImage.jpg");
                }

                request.AddObject(incidentmodel);

                //var response = await client.ExecuteTaskAsync(request);
                client.ExecuteAsync(request, (IRestResponse response, RestRequestAsyncHandle handler) => {
                    if (response.ResponseStatus == ResponseStatus.Error)
                    {
                        TaskCompletionSource.SetException(response.ErrorException);
                    }
                    else
                    {
                        TaskCompletionSource.SetResult(response);
                    }
                });

                return await TaskCompletionSource.Task;
            }
            catch (Exception e)
            {
                var message = e.Message;
                return null;
            }
        }        

        public async Task<IRestResponse> ReportIncident(ReportIncidentModel incident, /*System.IO.Stream file*/ byte[] file)
        {            
            // var client = new HttpClient();
         
            try
            {
                string jsonData = "{ \"Value\": \"message\" }";
                
                var data = "test=something";

                HttpClient client = new HttpClient();
                StringContent queryString = new StringContent(data);

                var content = new StringContent(jsonData, Encoding.UTF8, "text/json");
                HttpResponseMessage response = await client.PostAsync(new Uri("http://10.74.125.33/Asilungise/api/Incident/ReportIncident"), queryString);

                var result = await response.Content.ReadAsStringAsync();

                //var x = "";
            }

            catch (Exception en)
            {
                Console.WriteLine(en.Message);
            }

            return null;

            //try
            //{               
            //    var TaskCompletionSource = new TaskCompletionSource<IRestResponse>();

            //    var client = new RestClient("https://10.71.188.38/Asilungise");

            //    //var client = new RestClient("http://localhost:65156/");
            //    var request = new RestRequest("/api/Incident/ReportIncident", Method.POST);

            //    if (file != null)
            //    {
            //        request.AddFileBytes("Incident Image", file, "incidentImage.jpg");
            //    }

            //    request.AddObject(incident);

            //    //var response = await client.ExecuteTaskAsync(request);

            //    client.ExecuteAsync(request, (IRestResponse response, RestRequestAsyncHandle handler) => {

            //        if (response.ResponseStatus == ResponseStatus.Error)
            //        {
            //            TaskCompletionSource.SetException(response.ErrorException);
            //        }

            //        else
            //        {
            //            TaskCompletionSource.SetResult(response);
            //        }
            //    });

            //    return await TaskCompletionSource.Task;
            //}

            //catch (Exception ex)
            //{
            //    var error = ex.Message;
            //    return null;
            //}
        }
    }
}