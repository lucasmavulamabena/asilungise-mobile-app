﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Asilungiseni.Helper;
using Asilungiseni.Models;
using Asilungiseni.Services;

namespace Asilungiseni
{
    [Activity(Label = "Asilungise", MainLauncher = true)]
    public class LoginActivity : Activity
    {
        Intent intent;
        EditText etEmail;
        EditText etPassword;
        Button btnSignIn;
        Button btnRegister;
        Button btnForgotPassword;
        RememberMeHelper rememberMe;
        CheckBox checkRemember;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.LoginActivity);

            btnSignIn = FindViewById<Button>(Resource.Id.btnSignIn);
            btnRegister = FindViewById<Button>(Resource.Id.btnRegister);
            btnForgotPassword = FindViewById<Button>(Resource.Id.btnForgotPassword);
            checkRemember = FindViewById<CheckBox>(Resource.Id.checkRemember);
            checkRemember.Click += OnCheckClicked;
            checkRemember.Checked = false;
            etEmail = FindViewById<EditText>(Resource.Id.etEmail);
            etPassword = FindViewById<EditText>(Resource.Id.etPassword);
            etPassword.Text = string.Empty;
            etEmail.Text = string.Empty;

            rememberMe = new RememberMeHelper();

            if (rememberMe.Remembered())
            {
                Login login = rememberMe.RememberMe();
                etPassword.Text = login.Password;
                etEmail.Text = login.Username;
                checkRemember.Checked = true;
            }

            btnSignIn.Click += OnLoginClick;
            btnRegister.Click += OnRegisterClick;
            btnForgotPassword.Click += OnForgotPasswordClick;
        }

        private void OnCheckClicked(object sender, EventArgs e)
        {
            if (checkRemember.Checked)
            {                
                rememberMe.SaveCredentials(new Login { Username = etEmail.Text, Password = etPassword.Text });                
            }
        }

        private void OnLoginClick(object sender, EventArgs e)
        {
            if (LoginSuccess())

           // if (1 == 1)
            {
                intent = new Intent(this, typeof(ReportIncidentActivity));
                StartActivity(intent);
            }

            else
            {
                Toast.MakeText(this, "Please verify that username and password are correct", ToastLength.Long).Show();
            }
        }

        private void OnRegisterClick(object sender, EventArgs e)
        {
            intent = new Intent(this, typeof(RegisterActivity));         
            StartActivity(intent);
        }

        private void OnForgotPasswordClick(object sender, EventArgs e)
        {
            intent = new Intent(this, typeof(ForgotPasswordActivity));
            StartActivity(intent);
        }

        public bool LoginSuccess()
        {
            var userService = new UserService();

            var userModel = new UserModel
            {
                Email = etEmail.Text,
                Password = etPassword.Text          
            };

           // var result = Task.Run(async () => { return await userService.Login(userModel); }).Result;

            return (1 == 1);
            //return (result.StatusCode == System.Net.HttpStatusCode.OK || result.StatusCode == 0);

            //return (result.StatusCode == System.Net.HttpStatusCode.OK);         
            
        }
    }
}