﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Asilungiseni.Models
{
    public class ReportIncidentModel
    {
        //public System.IO.Stream File { get; set; }
        public string Region { get; set; }
        public string IncidentType { get; set; }
        public string Address { get; set; }       
        public string IncidentDescription { get; set; }
        public string Name { get; set; }
        public string Initials { get; set; }
        public string Surname { get; set; }
        public string ContactNumber { get; set; }
        public string EmailAddress { get; set; }
    }
}