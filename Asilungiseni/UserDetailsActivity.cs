﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics;
using Android.OS;
using Android.Provider;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Asilungiseni.Models;
using Asilungiseni.Services;
using Java.IO;

namespace Asilungiseni
{
    [Activity(Label = "UserDetailsActivity", MainLauncher = false)]
    public class UserDetailsActivity : Activity
    {
       // private Intent intent;
        ListView listView;       
        EditText userComent;      
        Button btnReportIncident;
        string itype;
        private ImageView  imgCamera, imgDialogCamera, imgDialogLibrary, imgDialogCancel;
        private Android.App.AlertDialog dialog;
        private TextView tvDialogCamera, tvDialogLibrary, tvDialogCancel;
        LocationService locationService;
        IncidentService IncidentService;
        File file;
        private byte[] imageBytes;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.UserDedails);
           // intent = new Intent(Application.Context, typeof(UserDetailsActivity));
            listView = FindViewById<ListView>(Resource.Id.listView1);
            //userName = FindViewById<EditText>(Resource.Id.name);
            //userContact = FindViewById<EditText>(Resource.Id.contacts);
           // userEmail = FindViewById<EditText>(Resource.Id.email);
            userComent = FindViewById<EditText>(Resource.Id.comment);
            btnReportIncident = FindViewById<Button>(Resource.Id.btnReportIncident);
            locationService = new LocationService();
            IncidentService = new IncidentService();

            itype = Intent.GetStringExtra("inctype") ?? "Data not available";

            if (IsCaptureCompatible())
            {
                CreateDirectoryForPictures();
                imgCamera = FindViewById<ImageView>(Resource.Id.imgIncident);
                imgCamera.Click += RetrieveImage;
            }

            btnReportIncident.Click += delegate {
                ReportIncident(itype);
            };

        }
        private void DisplayMediaDialog()
        {
            try
            {
                Android.App.AlertDialog.Builder mBuilder = new Android.App.AlertDialog.Builder(this);
                LayoutInflater inflater = (LayoutInflater)Application.Context.GetSystemService(Context.LayoutInflaterService);
                View view = inflater.Inflate(Resource.Layout.image_media_layout, null);

                tvDialogCamera = view.FindViewById<TextView>(Resource.Id.tvDialogCamera);
                tvDialogLibrary = view.FindViewById<TextView>(Resource.Id.tvDialogLibrary);
                tvDialogCancel = view.FindViewById<TextView>(Resource.Id.tvDialogCancel);
                imgDialogCamera = view.FindViewById<ImageView>(Resource.Id.imgDialogCamera);
                imgDialogLibrary = view.FindViewById<ImageView>(Resource.Id.imgDialogLibrary);
                imgDialogCancel = view.FindViewById<ImageView>(Resource.Id.imgDialogCancel);

                imgDialogCamera.Click += CaptureImage;
                tvDialogCamera.Click += CaptureImage;

                //imgDialogLibrary.Click += ReadFromLibrary;
                //tvDialogLibrary.Click += ReadFromLibrary;
                //imgDialogCancel.Click += CloseDialog;
                //tvDialogCancel.Click += CloseDialog;

                mBuilder.SetView(view);
                dialog = mBuilder.Create();
                dialog.Window.SetGravity(GravityFlags.Bottom);
                dialog.Show();
               // progress.Dismiss();
            }
            catch (Exception ex)
            {
                Android.Util.Log.Debug("ReportIncidentFragment: ", ex.Message);
            }
        }

        private void CaptureImage(object sender, EventArgs e)
        {
            try
            {

                dialog.Dismiss();
                Intent intent = new Intent(MediaStore.ActionImageCapture);
                
                var dir = new File(Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryPictures), "IncidentImages");

                if (!dir.Exists())
                {
                    dir.Mkdirs();
                }
                
                file = new File(dir, String.Format("myPhoto_{0}.jpg", Guid.NewGuid()));

                intent.PutExtra(MediaStore.ExtraOutput, Android.Net.Uri.FromFile(file));

                StartActivityForResult(intent, 0);
            }

            catch (Exception ex)
            {
                var message = ex.Message;
            }
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            try
            {
                switch (requestCode)
                {
                    case 0:
                        Intent mediaScanIntent = new Intent(Intent.ActionMediaScannerScanFile);
                        Android.Net.Uri contentUri = Android.Net.Uri.FromFile(file);
                        mediaScanIntent.SetData(contentUri);
                        SendBroadcast(mediaScanIntent);

                        Bitmap bitmap = BitmapFactory.DecodeFile(file.Path);

                        if (bitmap != null)
                        {                           
                            imgCamera.SetImageBitmap(bitmap);
                            //imageStream = new System.IO.MemoryStream();
                            imageBytes = System.IO.File.ReadAllBytes(file.Path);

                            //imageStream = new System.IO.MemoryStream();
                            //imageBytes = System.IO.File.ReadAllBytes(App._file.Path);
                            //App.bitmap.Compress(Bitmap.CompressFormat.Png, 23, imageStream);

                        }
                        break;
                    case 1:
                        imgCamera.SetImageURI(data.Data);
                        var imageStream = new System.IO.MemoryStream();

                        //string strPath = ImageFilePath.GetPath(this, data.Data);
                        //imageBytes = System.IO.File.ReadAllBytes(strPath);
                        //imageBytes = ImageFilePath.Compress(imageBytes);

                        break;
                    default:
                        Toast.MakeText(this, "Request code: " + requestCode, ToastLength.Long).Show();
                        break;
                }

            }
            catch (Exception ex)
            {
                string err = ex.Message;
            }
        }


        public void CreateDirectoryForPictures()
        {
            var dir = new File(Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryPictures), "IncidentImages");
            
            if (!dir.Exists())
            {
                dir.Mkdirs();
            }
        }

        private void RetrieveImage(object sender, EventArgs e)
        {
            DisplayMediaDialog();
        }

        public void ReportIncident(string incidenttype)
        {
            try
            {
            //    string inctype = Intent.GetStringExtra("incidenttype");

                var reportincidentmodel = new ReportIncidentModel
                {
                    Region = "Region",
                    IncidentType = itype,
                    Address = "Address",
                    IncidentDescription = "Description",
                    Name = "Name",
                    Surname = "Surname",
                    ContactNumber = "07254545458",
                    EmailAddress = "lucasmavulamabena@gmail.com"
                };         

                var result = Task.Run(async () => { return await IncidentService.ReportIncident(reportincidentmodel, null); }).Result;

                // var coordinates = Task.Run(async () => { return await locationService.getCurrentLocation(this); }).Result; //  Device issue
                // var coordinates = locationService.getCurrentLocation(this);
                // getCurrentCoordinates();

                Toast.MakeText(this, "Incident Reported.... ", ToastLength.Long).Show();
            }

            catch (Exception e)
            {
                Toast.MakeText(this, e.Message, ToastLength.Long).Show();
            }
           
        }

        private bool IsCaptureCompatible()
        {
            Intent intent = new Intent(MediaStore.ActionImageCapture);
            IList<ResolveInfo> availableActivities = PackageManager.QueryIntentActivities(intent, Android.Content.PM.PackageInfoFlags.MatchDefaultOnly);
            return availableActivities != null && availableActivities.Count > 0;
        }

    }
}