﻿using System;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Asilungiseni.Models;
using Asilungiseni.Services;

namespace Asilungiseni
{
    [Activity(Label = "ForgotPasswordActivity")]
    public class ForgotPasswordActivity : Activity
    {
        private EditText etEmail;
        private Intent intent;
        Button btnResendPsw;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.ForgotPasswordActivity);
            etEmail = FindViewById<EditText>(Resource.Id.etEmail);
            btnResendPsw = FindViewById<Button>(Resource.Id.btnResendPassword);
            btnResendPsw.Click += BtnResendPassword;
        }

        private void BtnResendPassword(object sender, EventArgs e)
        {
            try
            {
                var UserService = new UserService();

                var usermodel = new UserModel
                {             
                    Email = etEmail.Text,                   
                };

                // var result = Task.Run(async () => { return await UserService.SendPassword(usermodel); }).Result; // send email here

                Toast.MakeText(this, "User Name and password send succesfully...", ToastLength.Long).Show();

                intent = new Intent(this, typeof(LoginActivity));          
                StartActivity(intent);
            }
            catch (Exception ex)
            {
                Toast.MakeText(this, "Error occured: " + ex.Message, ToastLength.Long).Show();
            }

        }
    }
}