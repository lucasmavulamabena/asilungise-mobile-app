﻿using System;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Asilungiseni.Models;
using Asilungiseni.Services;

namespace Asilungiseni
{
    [Activity(Label = "RegisterActivity")]
    public class RegisterActivity : Activity
    {

        private EditText etEmail, etName, etContact, etPassword;
        Intent intent;
        Button btnRegister;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.RegisterActivity);
            btnRegister = FindViewById<Button>(Resource.Id.btnRegister);       

            etEmail = FindViewById<EditText>(Resource.Id.etEmail);
            etName = FindViewById<EditText>(Resource.Id.etName);
            etContact = FindViewById<EditText>(Resource.Id.etContactNumber);
            etPassword = FindViewById<EditText>(Resource.Id.etPassword);

            btnRegister.Click += RegisterUser;
           // Create your application here
        }
               

        private void RegisterUser(object sender, EventArgs e)
        {
            try
            {
                var UserService = new UserService();

                var usermodel = new UserModel
                {
                    Name = etName.Text,
                    Contact = etContact.Text,
                    Email = etEmail.Text,
                    Password = etPassword.Text
                };

                var result = Task.Run(async () => { return await UserService.RegisterUser(usermodel); }).Result;

                Toast.MakeText(this, "User Registered Successfully..", ToastLength.Long).Show();

                intent = new Intent(this, typeof(LoginActivity));
                // intent.PutExtra("username", etUserID.Text);
                //rememberMe.SaveLoginState(true);
                StartActivity(intent);
            }
            catch (Exception ex)
            {
                Toast.MakeText(this,"Error occured: " + ex.Message, ToastLength.Long).Show();
            }
           
        }
    }
}