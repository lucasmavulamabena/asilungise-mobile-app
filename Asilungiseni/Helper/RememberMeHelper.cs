﻿
using Android.App;
using Android.Content;
using Android.Preferences;
using Asilungiseni.Models;

namespace Asilungiseni.Helper
{
    class RememberMeHelper
    {
        ISharedPreferences sharedPrefs;
        ISharedPreferencesEditor editor;
        public RememberMeHelper()
        {
            sharedPrefs = PreferenceManager.GetDefaultSharedPreferences(Application.Context);
            editor = sharedPrefs.Edit();
        }

        public void SaveCredentials(Login login)
        {
            editor.PutString("userID", login.Username);
            editor.PutString("password", login.Password);
            editor.Apply();
        }        

        public void SaveLoginState(bool isLoggedIn)
        {
            editor.PutBoolean("isLoggedIn", isLoggedIn);
            editor.Apply();
        }

        public bool getLoginState()
        {
            return sharedPrefs.GetBoolean("isLoggedIn", false);
        }

        public Login RememberMe()
        {
            return new Login { Username = sharedPrefs.GetString("userID", null), Password = sharedPrefs.GetString("password", null) };

        }

        public bool Remembered()
        {
            var remembered = new Login { Username = sharedPrefs.GetString("userID", null), Password = sharedPrefs.GetString("password", null) };
            var rm = remembered.Username != null || remembered.Password != null;
            return rm;
        }
    }
}