﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Asilungiseni.Models;
using Asilungiseni.Services;

namespace Asilungiseni
{
    [Activity(Label = "ReportIncidentActivity", MainLauncher = false)]
    public class ReportIncidentActivity : Activity
    {
        private Intent intent;
        private string incidenttype;
        ListView listView;
       // EditText userName;
       // EditText userContact;
       // EditText userEmail;
       
        //EditText userComent;
        Button btnReportIncident;
        //private LocationService locationService;
        List<String> it = new List<String>();

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.ReportIncident);
            intent = new Intent(Application.Context, typeof(ReportIncidentActivity));
            listView = FindViewById<ListView>(Resource.Id.listView1);
          //  userName = FindViewById<EditText>(Resource.Id.name);
           // userContact = FindViewById<EditText>(Resource.Id.contacts);
            //userEmail = FindViewById<EditText>(Resource.Id.email);
            //userComent = FindViewById<EditText>(Resource.Id.comment);
            btnReportIncident = FindViewById<Button>(Resource.Id.Next);
            listView.ItemClick += OnItemClick;

            btnReportIncident.Click += delegate {               
                RenderUserDetailsPage();
            };

            listView.Adapter = new ArrayAdapter<String>(this, Android.Resource.Layout.SimpleListItemChecked, GetincindentTypes());        
            // Create your application here
        }

        public void RenderUserDetailsPage()
        {
            intent = new Intent(this, typeof(UserDetailsActivity));
            intent.PutExtra("inctype", incidenttype);

            StartActivity(intent);
        }

        private void OnItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            // var incidenttype = e.Position;
           // var Nextactivity = new Intent(this, typeof(UserDetailsActivity));
            //Nextactivity.PutExtra("inctype", "Water");
            incidenttype = it[e.Position];

            //e.View.Selected = true;
            // string text = Nextactivity.GetStringExtra("inctype") ?? "Data not available";

            //Toast.MakeText(this, it[e.Position], ToastLength.Long).Show();          
        }

        List<string> GetincindentTypes()
        {
            var IncidentService = new IncidentService();
            it.Clear();

            it.Add("Water");
            it.Add("Sewage"); it.Add("Potholes"); it.Add("Street Lights"); it.Add("Electricity"); it.Add("Trees");
            //var result = Task.Run(async () => { return await IncidentService.GetIncidentTypes(); }).Result;

            //foreach (var incidenttype in result.ToList())
            //{
            //    it.Add(incidenttype);
            //}

            return it;
        }

      
    }
}